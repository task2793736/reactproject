import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  let  [Counter, setCounter] = useState(0)
  const add = ()=>{
    if(Counter == 20){
    setCounter(Counter)
    }else{
      Counter = Counter + 1
      setCounter(Counter)
    }
  }
 
  const remove = ()=>{
    if(Counter == 0){
      setCounter(Counter)
    }else{
      Counter = Counter - 1
      setCounter(Counter)
    }
  }

  return (
    <>
      <h2>chai Aur react</h2>
      <h2>Counter value: {Counter}</h2>
      <button 
      onClick={add}>Add Value {Counter}</button>
      <br />
      <button
      onClick={remove}>Remove Value {Counter}</button>
     
    </>
  )
}

export default App
